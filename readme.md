##Settings Utility

--------
	
The Settings Utility application is a software program that helps create SQL scripts for easy migration of online banking clients from one platform to another one.

**Key Features:** eliminates repetitive task, eliminates errors creating SQL scripts, improve efficiency and user friendly

**Installation:**

You need node.js and npm. At the root of the project type:

```node
npm install
```

**Run Project:**

```node
npm run start
```

**Software Used:**

1. CSS3 (BootStrap http://getbootstrap.com)
1. HTML5 (FileSystem API, drag-n-drop)
1. Angularjs (https://angularjs.org) use for MVVM
1. Yeoman (http://yeoman.io) workflow tooling for kickstarting new projects
1. TypeScript (http://www.typescriptlang.org) 

--------
**Example:**

- Real-time creation of SQL scripts
- In browser file reading and creation

![Setting_Utility](Setting_Utility.gif)