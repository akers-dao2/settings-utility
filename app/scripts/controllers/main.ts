'use strict';

/// <reference path="ConvertCSV.ts"/>

interface angularJS{
    module:any;
}

declare var angular:angularJS;
declare var $;

import csvtojson = module(convertCSVToJSON);

angular.module('SettingsUtilityApp')
    .controller('MainCtrl', function ($scope, $location) {

        if (!(Blob && FileReader && window.URL)) {
            $location.path('/error')
        }


        $('.carousel').carousel({
            interval: false
        });


        var tojson = new csvtojson.LoadFiles("#drop", $scope);

        $scope.back = function () {
            $('.carousel').carousel('prev');
            $scope.progress = '0';
        }

        $scope.openTableRow = {
            func: function (index) {
                if (index === 1) {
                    this.settingsOpen = !this.settingsOpen;
                    (this.settingsOpen) ? this.sFolder = 'icon-folder-open' : this.sFolder = 'icon-folder-close';

                } else {
                    this.onlineformsOpen = !this.onlineformsOpen;
                    (this.onlineformsOpen) ? this.oFolder = 'icon-folder-open' : this.oFolder = 'icon-folder-close';
                }
            },
            settingsOpen: false,
            onlineformsOpen: false,
            oFolder: 'icon-folder-close',
            sFolder: 'icon-folder-close'
        }

    });


angular.module('SettingsUtilityApp')
    .controller('ErrorCRTL', function ($scope) {
        $('#myModal').modal()
});
