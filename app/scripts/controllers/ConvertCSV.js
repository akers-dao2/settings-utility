'use strict';
var convertCSVToJSON;
(function (convertCSVToJSON) {
    function errorHandler(e) {
        var msg = '';
        switch(e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
                msg = 'QUOTA_EXCEEDED_ERR';
                break;
            case FileError.NOT_FOUND_ERR:
                msg = 'NOT_FOUND_ERR';
                break;
            case FileError.SECURITY_ERR:
                msg = 'SECURITY_ERR';
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                msg = 'INVALID_MODIFICATION_ERR';
                break;
            case FileError.INVALID_STATE_ERR:
                msg = 'INVALID_STATE_ERR';
                break;
            default:
                msg = 'Unknown Error';
                break;
        }
        ;
        console.log('Error: ' + msg);
    }
    var LoadFiles = (function () {
        function LoadFiles(selector, scope) {
            this.scope = scope;
            this.drop = document.querySelector(selector);
            this.addDropEvent();
            this.upload = document.querySelector(selector + ' input[type="file"]');
            this.removeFileChangeEvent();
            this.addFileChangeEvent();
            this.onlineforms = false;
        }
        Object.defineProperty(LoadFiles.prototype, "onlineforms", {
            get: function () {
                return this._onlineforms;
            },
            set: function (val) {
                this._onlineforms = val;
            },
            enumerable: true,
            configurable: true
        });
        
        LoadFiles.prototype.addFileChangeEvent = function () {
            var _this = this;
            this.upload.addEventListener('change', function (e) {
                _this.dropfn(e);
            }, false);
        };
        LoadFiles.prototype.removeFileChangeEvent = function () {
            var _this = this;
            this.upload.removeEventListener('change', function (e) {
                _this.dropfn(e);
            }, false);
        };
        LoadFiles.prototype.addDropEvent = function () {
            var _this = this;
            this.drop.addEventListener('dragover', this.cancel, false);
            this.drop.addEventListener('dragenter', this.cancel, false);
            this.drop.addEventListener('drop', function (e) {
                _this.dropfn(e);
            }, false);
        };
        LoadFiles.prototype.cancel = function (e) {
            if(e.preventDefault) {
                e.stopPropagation();
                e.preventDefault();
                e.dataTransfer.dropEffect = 'copy';
            }
            return false;
        };
        LoadFiles.prototype.dropfn = function (e) {
            var _this = this;
            e.stopPropagation();
            e.preventDefault();
            this.f = e.target.files || e.dataTransfer.files;
            console.log(this.f);
            if(this.f[0].type === "text/csv" || this.f[0].type === "application/vnd.ms-excel" || this.f[0].type === "text/vnd.ms-excel") {
                (this.f[0].name.search(/onlineform/i) >= 0) ? this.onlineforms = true : this.onlineforms = false;
                this.scope.error = false;
                var reader = new FileReader();
                reader.onload = function (e) {
                    console.log(e.target.result);
                    var _result = _this.addHeading(e.target.result);
                    new CsvToJson(_result, function (e) {
                        _this.sql = new SQLStatements({
                            data: e,
                            scope: _this.scope,
                            onlineforms: _this.onlineforms,
                            file: _this.f[0],
                            settingHeader: _this.scope.headerExist.sCheckBox,
                            onlineformsHeader: _this.scope.headerExist.oCheckBox
                        });
                    });
                };
                reader.onerror = function (e) {
                    console.log('e.target.result');
                };
                reader.readAsText(this.f[0]);
            } else {
                this.scope.$apply(function () {
                    _this.scope.error = true;
                    _this.scope.message = "The only types allowed are text/csv";
                });
                throw new Error("The only types allowed are text/csv");
            }
        };
        LoadFiles.prototype.addHeading = function (e) {
            var heading;
            if(this.onlineforms) {
                heading = "id,retail,business\n";
            } else {
                heading = "name,value\n";
            }
            return heading.concat(e);
        };
        LoadFiles.prototype.download = function () {
            var _this = this;
            window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
            window.requestFileSystem(window.TEMPORARY, 5 * 1024 * 1024, function (e) {
                _this.onInit(e);
            }, errorHandler);
        };
        LoadFiles.prototype.onInit = function (e) {
            e.root.getFile('log.txt', {
                create: true
            }, function (fileEntry) {
                fileEntry.createWriter(function (fileWriter) {
                    fileWriter.onwriteend = function (e) {
                        console.log('Write completed.');
                    };
                    fileWriter.onerror = function (e) {
                        console.log('Write failed: ' + e.toString());
                    };
                    var blob = new Blob([
                        'Lorem Ipsum'
                    ], {
                        type: 'text/plain'
                    });
                    fileWriter.write(blob);
                }, errorHandler);
            }, errorHandler);
        };
        return LoadFiles;
    })();
    convertCSVToJSON.LoadFiles = LoadFiles;    
    var CsvToJson = (function () {
        function CsvToJson(csv, callback) {
            this.arrayOfObject = $.csv.toObjects(csv, {
                headers: true
            }, function (err, data) {
                callback(data);
            });
        }
        return CsvToJson;
    })();
    convertCSVToJSON.CsvToJson = CsvToJson;    
    var SQLStatements = (function () {
        function SQLStatements(sqlData) {
            var _this = this;
            this.sql = [];
            this.sqlData = sqlData;
            var counter = 100 / sqlData.data.length;
            var fileName;
            var unit = 100 / sqlData.data.length;
            sqlData.data.forEach(function (d) {
                var q;
                if(!sqlData.onlineforms) {
                    q = "update settings set value = '" + d.value + "' where name = '" + d.name + "'";
                    fileName = 'script.sql';
                } else {
                    q = "update onlineforms set AvailableRetail = '" + d.retail + "' ,AvailableBusiness= '" + d.business + "' where id = '" + d.id + "'";
                    fileName = 'onlineforms.sql';
                }
                _this.sql.push(q);
                sqlData.scope.$apply(function () {
                    sqlData.scope.progress = counter;
                    counter = unit + counter;
                });
            });
            
            if(sqlData.settingHeader === 'icon-check' || sqlData.onlineformsHeader === 'icon-check'){
                   _this.sql.splice(0,1); 
                }
            window.setTimeout(function () {
                $('.carousel').carousel('next');
            }, 100);
            sqlData.scope.$apply(function () {
                sqlData.scope.txt = fileName;
                var blob = new Blob([
                    _this.results()
                ], {
                    type: 'text/plain'
                });
                sqlData.scope.ref = URL.createObjectURL(blob);
                sqlData.scope.file = sqlData.file.name.slice(0, -4) + ".sql";
            });
        }
        SQLStatements.prototype.standardScripts = function () {
            return [
                "UPDATE DetailFields SET IsRate = 1 WHERE (Field = 'MiscAmount4')", 
                "Update Categories set [Description] = category where [Description] is Null or [Description] = ''", 
                "update settings set value  = 'FALSE' where name = 'ShowBusinessDemo'", 
                "update settings set value  = 'FALSE' where name = 'ShowPersonalDemo'"
            ];
        };
        SQLStatements.prototype.results = function () {
            var _this = this;
            if(!this.sqlData.onlineforms) {
                this.standardScripts().forEach(function (item) {
                    _this.sql.push(item);
                });
            }
            return this.sql.join('\n');
        };
        return SQLStatements;
    })();    
})(convertCSVToJSON || (convertCSVToJSON = {}));

