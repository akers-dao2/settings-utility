/**
 * Created with JetBrains WebStorm.
 * User: falosakers
 * Date: 6/11/13
 * Time: 8:56 PM
 * To change this template use File | Settings | File Templates.
 */

'use strict';

module convertCSVToJSON{
    //Error handling function
    declare var FileError;

    function errorHandler(e) {
        var msg = '';

        switch (e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
                msg = 'QUOTA_EXCEEDED_ERR';
                break;
            case FileError.NOT_FOUND_ERR:
                msg = 'NOT_FOUND_ERR';
                break;
            case FileError.SECURITY_ERR:
                msg = 'SECURITY_ERR';
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                msg = 'INVALID_MODIFICATION_ERR';
                break;
            case FileError.INVALID_STATE_ERR:
                msg = 'INVALID_STATE_ERR';
                break;
            default:
                msg = 'Unknown Error';
                break;
        }
        ;

        console.log('Error: ' + msg);
    }


    //load files drop or upload
    export class LoadFiles {
        drop:Node;
        upload:Node;
        f:string;
        sqls:string;
        private _onlineforms:bool;

        constructor(selector:string, public scope:any) {
            this.drop = document.querySelector(selector); //identifies the drop zone
            this.addDropEvent();
            this.upload = document.querySelector(selector + ' input[type="file"]')
            this.addFileChangeEvent();
            this.onlineforms = false;
        }

        get onlineforms():bool {
            return this._onlineforms;
        }

        set onlineforms(val:bool) {
            this._onlineforms = val;
        }

        addFileChangeEvent() {
            this.upload.addEventListener('change', (e)=> {
                this.dropfn(e)   //
            }, false);
        }

        addDropEvent() {
            this.drop.addEventListener('dragover', this.cancel, false);
            this.drop.addEventListener('dragenter', this.cancel, false);
            this.drop.addEventListener('drop', (e)=> {
                this.dropfn(e)   //
            }, false);
        }

        //prevents the from opening up via the browser window
        //when the file is the drop area
        cancel(e:Event) {
            if (e.preventDefault) {
                e.stopPropagation();
                e.preventDefault();
                e.dataTransfer.dropEffect = 'copy'
            }
            return false;
        }

        //handles the drop or file input.

        dropfn(e:Event) {
            e.stopPropagation();
            e.preventDefault();
            this.f = e.target.files || e.dataTransfer.files;
            console.log(this.f);
            if (this.f[0].type === "text/csv" || this.f[0].type === "application/vnd.ms-excel") {
                (this.f[0].name === "onlineform.csv") ? this.onlineforms = true : this.onlineforms = false;

                //disable error alert
                this.scope.error = false;

                var reader = new FileReader();
                reader.onload = (e)=> {
                    console.log(e.target.result);
                    var _result = this.addHeading(e.target.result);
                    new CsvToJson(_result, (e)=> {
                        this.sql = new SQLStatements({
                            data:e,
                            scope:this.scope,
                            onlineforms:this.onlineforms,
                            file:this.f[0]
                        });
//                    console.log(this.sql.results());
                    });


                };
                reader.onerror = function (e) {
                    console.log('e.target.result');
                };

                reader.readAsText(this.f[0]);
            }
            else{
                this.scope.$apply(()=> {
                    this.scope.error = true;
                    this.scope.message = "The only types allowed are text/csv";
                });

                throw new Error("The only types allowed are text/csv")

            }



        }

        addHeading(e) {
            var heading;

            if(this.onlineforms){
                heading = "id,retail,business\n";
            }
            else{
                heading = "name,value\n";
            }
            return heading.concat(e);

        }

        download() {
            window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;

            window.requestFileSystem(window.TEMPORARY, 5 * 1024 * 1024, (e)=> {
                this.onInit(e)
            }, errorHandler)

        }

        onInit(e) {
            e.root.getFile('log.txt', {create: true}, function (fileEntry) {

                fileEntry.createWriter(function (fileWriter) {

                    fileWriter.onwriteend = function (e) {
                        console.log('Write completed.');
                    };

                    fileWriter.onerror = function (e) {
                        console.log('Write failed: ' + e.toString());
                    };

                    // Create a new Blob and write it to log.txt.
                    var blob = new Blob(['Lorem Ipsum'], {type: 'text/plain'});

                    fileWriter.write(blob);

                }, errorHandler);

            }, errorHandler);

        }

    }
    interface $csv{
        csv:any;
    }
    declare var $:$csv;

    export class CsvToJson {
        arrayOfObject:{}[];

        constructor(csv:string, callback:(e)=>void) {
            this.arrayOfObject = $.csv.toObjects(csv, {
                headers: true
            }, function (err, data) {
//                console.log(err);
                callback(data);
            });
        }
    }

    interface importData{
        value:string;
        name:string;
    }

    interface sqlData{
        data:importData[];
        scope:any;
        onlineforms:boolean;
        file:any;
    }

    class SQLStatements {
        sql:string[];
        sqlData:sqlData;

        constructor(sqlData) {
            this.sql = [];
            this.sqlData = sqlData;
            var counter = 100 / sqlData.data.length;
            var fileName;
            var unit = 100 / sqlData.data.length;
            sqlData.data.forEach((d)=> {
                var q;
                if (!sqlData.onlineforms) {
                    q = "update settings set value = '" + d.value + "' where name = '" + d.name + "'";
                    fileName = 'script.sql';
                }
                else {
                    q = "update onlineforms set AvailableRetail = '" + d.retail + "' ,AvailableBusiness= '" + d.business + "' where id = '" + d.id + "'";
                    fileName = 'onlineforms.sql';
                }

                this.sql.push(q);
                sqlData.scope.$apply(()=> {
                    sqlData.scope.progress = counter;
                    counter = unit + counter;
                })

            });
            window.setTimeout(function () {
                $('.carousel').carousel('next');
            }, 100);

            //to-do add error handling
            sqlData.scope.$apply(()=> {
                sqlData.scope.txt = fileName;
                var blob = new Blob([this.results()], {type: 'text/plain'});
                sqlData.scope.ref = URL.createObjectURL(blob);
                sqlData.scope.file = sqlData.file.name.slice(0,-4) + ".sql";
            })
        }
        standardScripts(){
            return ["UPDATE DetailFields SET IsRate = 1 WHERE (Field = 'MiscAmount4')",
            "Update Categories set [Description] = category where [Description] is Null or [Description] = ''",
            "update settings set value  = 'FALSE' where name = 'ShowBusinessDemo'",
            "update settings set value  = 'FALSE' where name = 'ShowPersonalDemo'"]
        }
        results() {
            if(!this.sqlData.onlineforms){
                this.standardScripts().forEach((item)=>{
                    this.sql.push(item);
                })
            }
            return this.sql.join('\n');
        }

    }
}