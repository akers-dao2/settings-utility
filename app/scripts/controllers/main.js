'use strict';
var csvtojson = convertCSVToJSON;
angular.module('SettingsUtilityApp').controller('MainCtrl', function ($scope, $location) {
    if(!(Blob && FileReader && window.URL)) {
        $location.path('/error');
    }
    $('.carousel').carousel({
        interval: false
    });

    
    (function (json) {
        new csvtojson.LoadFiles("#drop", $scope);
    }())
    
    $scope.back = function () {
        $('.carousel').carousel('prev');
        $scope.progress = '0';
        new csvtojson.LoadFiles("#drop", $scope);
    };
    $scope.openTableRow = {
        func: function (index) {
            if(index === 1) {
                this.settingsOpen = !this.settingsOpen;
                (this.settingsOpen) ? this.sFolder = 'icon-folder-open' : this.sFolder = 'icon-folder-close';
            } else {
                this.onlineformsOpen = !this.onlineformsOpen;
                (this.onlineformsOpen) ? this.oFolder = 'icon-folder-open' : this.oFolder = 'icon-folder-close';
            }
        },
        settingsOpen: false,
        onlineformsOpen: false,
        oFolder: 'icon-folder-close',
        sFolder: 'icon-folder-close'
    };
    
    $scope.headerExist = {
        func: function(index){
            if(index === 1) {
                this.settingIsHeader = !this.settingIsHeader;
                (this.settingIsHeader) ? this.sCheckBox = 'icon-check' : this.sCheckBox = 'icon-check-empty';
            } else {
                this.onlineFormIsHeader = !this.onlineFormIsHeader;
                (this.onlineFormIsHeader) ? this.oCheckBox = 'icon-check' : this.oCheckBox = 'icon-check-empty';
            }
        },
        settingIsHeader: false,
        onlineFormIsHeader: false,
        oCheckBox: 'icon-check-empty',
        sCheckBox: 'icon-check-empty'
        
    }
});

angular.module('SettingsUtilityApp').controller('ErrorCRTL', function ($scope) {
    $('#myModal').modal();
});
//@ sourceMappingURL=main.js.map
