(function () {
    'use strict';

    angular.module('SettingsUtilityApp', [])
        .config(function ($routeProvider, $compileProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'views/main.html',
                    controller: 'MainCtrl'
                })
                .when('/error', {
                    templateUrl: 'views/error.html',
                    controller: 'ErrorCRTL'
                })
                .otherwise({
                    redirectTo: '/'
                });

            $compileProvider.urlSanitizationWhitelist(/^\s*(blob?|ftp|mailto|file):/);
        });

}());